# GIT SUMMARY    
_Git is designed for collaborating work among programmers and  used to track changes in any set of files during software development._

#### Required Vocabulary
**Repository** - collection of  files and folders.  
**Commit** - taking snapshot of the files as they exist at that moment just before changes in repository.  
**Push** - syncing your commits.  
**Branch** - pointer to the head of group of commits.  
**Merge** - integrating the branch with main code.  
**Clone** - making a copy of main code into local machine.   
**Fork** - copy repository with your username.  

Git has three main states:
* Modified  
* Staged
* Committed

**Modified** : It is _Working Tree_ which consists of files you are working on.Changed the file but committed.    
**Staged** : Marked the changes but not committed.  
**Commited** : Save your changes to local repository.  

>Other states:
>* **Workspace** :  The tree of repo in which you make all the changes through Editor(s).
>* **Staging** :  All the staged files are saved in this repository. 
>* **Local Repository** :  All the committed files are saved in this. 
>* **Remote Repository** :  the copy of local repository on some server, changes made on loacl repo doesn't affect this repository.

####Basic commands of git work flow:
* Clone - $ git clone <link-to-repository>   
* Create Branch - $ git checkout -b <your-branch-name>  
* Add - $ git add  
* Commit - $ git commit -sv  
* Push - $ git push origin <branch-name>   

![Alt]http://www.cloudturbine.com/wordpress/wp-content/uploads/2016/08/git_workflow.png
