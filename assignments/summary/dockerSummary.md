# DOCKER 
>Docker is a program for developers to develop, and run applications with containers.    

*_Container_* - A container is a runnable instance of an image. We can create, start, stop, move, or delete a container using the Docker API or CLI.  
*_DockersHub_* - Docker Hub is like GitHub but for docker images and containers.
### DOCKER IMAGE
 A Docker image is a file, comprised of multiple layers, that is used to execute code in a Docker container. It contains everything needed to run applications as a container.   This includes:
* code  
* runtime  
* libraries  
* environment variables  
* configuration files  
The image can then be deployed to any Docker environment and as a container.

### DOCKER TERMINOLOGY
*docker ps* : Allows us to view all the containers that are running on the Docker Host.  
*docker start* : Starts any stopped container(s).  
*docker stop* : Stops any running container(s).  
*docker rm* : Deletes the containers.  

#### DOCKER WORFLOW DIAGRAM:
![Alt][https://tin6150.github.io/psg/fig/docker-workflow-cmd.png]

